import codecs
import socket
from string import printable
from threading import Thread
from config import *

context_counter = 0

class Context:
    def __init__(self):
        global context_counter
        self.current_user = None
        self.socket = None
        self.id = context_counter
        context_counter += 1

    def send_to_socket(self, msg):
        if self.socket:
            try:
                self.socket.send(codecs.encode(msg + "\n", "utf8"))
            except Exception:
                pass

    def check_current_user(self, login):
        return self.current_user is not None and self.current_user['login'] == login


class QuitCommand(BaseException):
    pass


def handle_client(server, context):
    socket = context.socket
    try:
        while True:
            request = socket.recv(16384).strip()
            request = codecs.decode(request, "utf8")
            result, answer = server.execute(context, request)
            if result:
                prefix = b"+ "
            else:
                prefix = b"- "
            answer = codecs.encode(answer, "utf8")
            socket.send(b"%s%s\n" % (prefix, answer))
    except QuitCommand:
        pass
    except Exception as e:
        print("[C%d] Error while handling the socket (Reason %s)" % (context.id, e))

    server.destroy_context(context)
    try:
        socket.close()
        print("[C%d] Connection closed" % context.id)
    except Exception:
        pass

def grey(msg): 
    return '\033[90m{}\033[00m'.format(msg)
def red(msg): 
    return '\033[91m{}\033[00m'.format(msg)
def green(msg): 
    return '\033[92m{}\033[00m'.format(msg)
def yellow(msg): 
    return '\033[93m{}\033[00m'.format(msg)
def blue(msg): 
    return '\033[94m{}\033[00m'.format(msg)
def purple(msg): 
    return '\033[95m{}\033[00m'.format(msg)
def cyan(msg): 
    return '\033[96m{}\033[00m'.format(msg)

colors = {'/grey' : grey, '/red' : red, '/green' : green, '/yellow' : yellow, '/blue' : blue, '/purple' : purple, '/cyan' : cyan}

class Server:
    def __init__(self, config):
        self.config = config
        self.contexts = []
        if config['userdb']:
            self.users = load_userdb_from_file(config['userdb'])
        else:
            self.users = UserDB()

    def new_context(self):
        result = Context()
        self.contexts.append(result)
        return result

    def destroy_context(self, context):
        self.contexts = list(filter(lambda c : c.id != context.id, self.contexts))

    def execute(self, context, line):
        if line == "":
            return False, "Empty request"
        command_and_args = line.split()
        command = command_and_args[0]
        args = command_and_args[1:]
        if command == "register":
            if len(args) != 2:
                return False, "Register command format : register <username> <password>"
            login, password = args
            login = login.strip()
            password = password.strip()
            alpha = printable
            for i in login:
                if not i in alpha:
                    return False, "Wrong character"
            if login in self.users.user_list():
                return False, "User already exist"
            self.users.add_user(login, password, False)
            user = self.users.get(login)
            context.current_user = user
            return True, "Account created, welcome {login}".format(login=login)
        if command == "mp":
            if len(args) < 1:
                return False, "Mp command format : mp <username> <message>"
            login = args[0]
            message = " ".join(args[1:])
            if not self.users.get(login):
                return False, "User {} does not exist".format(login)
            alpha = printable
            if message == "":
                message = "nothing"
            else:
                for i in message:
                    if not i in alpha:
                        return False, "Wrong character"
            if context.current_user:
                username = context.current_user['login']
            else:
                username = "Anonymous coward"
            if len(message.split(None, 1))>1:
                color, message_color = message.split(None, 1)
                if color in colors.keys():
                    message = colors[color](message_color)
            msg = "%s whispers \"%s\"" % (username, message)
            test = False
            for c in self.contexts:
                if c.current_user != None and c.current_user['login'] == login:
                    c.send_to_socket(msg)
                    test = True
            if test :
                return True, "Message sent"
            return False, "User not connected"
        if command == "login":
            if len(args) != 2:
                return False, "Login command format : login <username> <password>"
            login, password = args
            login = login.strip()
            password = password.strip()
            try_user = self.users.get(login)
            if try_user and try_user['password'] == password:
                context.current_user = try_user
                return True, ("Welcome %s" % login)
            else:
                return False, "Invalid user or password"
        elif command == "logout":
            if context.current_user:
                login = context.current_user['login']
                context.current_user = None
                return True, ("Bye %s" % login)
            else:
                return False, "You are not currently logged in"

        elif command == "whoami":
            if context.current_user:
                login = context.current_user['login']
                return True, ("You are %s, silly" % login)
            else:
                return False, "You are not currently logged in"

        elif command == "list":
            if context.current_user:
                logins = self.users.user_list()
                result = []
                for l in logins:
                    relevant_contexts = filter(lambda c: c.check_current_user(l), self.contexts)
                    connected = list(relevant_contexts) != []
                    if connected:
                        result.append("*%s" % l)
                    elif context.current_user['admin']:
                        result.append("%s" % l)
                return True, ", ".join(result)
            else:
                return False, "You are not currently logged in"

        elif command == "print":
            if args == []:
                msg = "nothing"
            else:
                alpha = printable
                print_cmd, msg = line.split(None, 1)
                for i in msg:
                    if not i in alpha:
                        return False, "Wrong character"
                if len(msg.split(None, 1))>1:
                    color, message_color = msg.split(None, 1)
                    if color in colors.keys():
                        msg = colors[color](message_color)
                msg = "\"%s\"" % msg

            if context.current_user:
                username = context.current_user['login']
            else:
                username = "Anonymous coward"
            msg = "%s says %s" % (username, msg)
            print(msg)
            for c in self.contexts:
                c.send_to_socket(msg)
            return True, "Message posted"

        elif command == "help":
            return True, "Valid commands are: help, list, login, register, logout, print, mp, quit, whoami"

        elif command == "quit":
            raise QuitCommand

        else:
            return False, "Invalid command"

    def run(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        address_and_port = self.config['address'], self.config['port']
        server_socket.bind(address_and_port)
        server_socket.listen(5)
        print("[S] Server listening on %s:%d" % address_and_port)

        while True:
            context = self.new_context()
            try:
                s, client_address = server_socket.accept()
                context.socket = s
                print("[C%d] Connection accepted from %s" % (context.id, client_address))
                t = Thread(target=handle_client, args=(self, context))
                t.start()
            except Exception as e:
                print("[C%d] Failed (Reason: %s)" % (context.id, e))
                self.destroy_context(context)


if __name__ == "__main__":
    try:
        config = load_config_from_file("config")
    except Exception:
        config = Config()
    server = Server(config)
    try:
        server.run()
    except KeyboardInterrupt:
        print()
        pass
